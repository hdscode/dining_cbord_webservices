@ECHO OFF
REM Stop a Service
SET count=0
sc \\hdsvcnet01.hds.int.colorado.edu stop cborddataservice.generic
:StopDataService1
                ping localhost -n 5 > nul
                IF %count% GEQ 24 GOTO StopFailure
                SET /a count=%count% + 1
                sc \\hdsvcnet01.hds.int.colorado.edu query cborddataservice.generic | find /i "stopped" || GOTO StopDataService1

                ECHO service_name successfully stopped.

REM Start a Service
SET count=0
sc \\hdsvcnet01.hds.int.colorado.edu start cborddataservice.generic
:StartDataService1
               ping localhost -n 5 > nul
               IF %count% GEQ 24 GOTO StartFailure
               SET /a count=%count% + 1
               sc \\hdsvcnet01.hds.int.colorado.edu query cborddataservice.generic | find /i "running" || GOTO StartDataService1

               ECHO service_name started successfully.

GOTO RestartAppPool

:StopFailure
echo Failed to stop service
REM Send Email with failure
GOTO End

:StartFailure
echo Failed to start service
REM Send Email with failure
GOTO End

:RestartAppPool

IF NOT EXIST c:\windows\system32\iisapp.vbs GOTO IIS7

cscript.exe c:\windows\system32\iisapp.vbs /a CBORDPOSWebServiceAppPool /r
REM Send Email that everything worked
GOTO End

:IIS7

C:\Windows\System32\inetsrv\appcmd.exe STOP APPPOOL CBORDPOSWebServiceAppPool
C:\Windows\System32\inetsrv\appcmd.exe START APPPOOL CBORDPOSWebServiceAppPool

C:\Windows\System32\inetsrv\appcmd.exe STOP APPPOOL CBORDWebDataAppPool
C:\Windows\System32\inetsrv\appcmd.exe START APPPOOL CBORDWebDataAppPool

C:\Windows\System32\inetsrv\appcmd.exe STOP APPPOOL CStoreAppPool
C:\Windows\System32\inetsrv\appcmd.exe START APPPOOL CStoreAppPool

C:\Windows\System32\inetsrv\appcmd.exe STOP APPPOOL NetNutritionAdminAppPool
C:\Windows\System32\inetsrv\appcmd.exe START APPPOOL NetNutritionAdminAppPool

REM Send Email that everything worked

:End